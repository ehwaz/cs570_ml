
import os, csv, sys
import time

class CSVData:
	def __init__(self):
		self.validInCSV = dict()
		self.validSolCSV = dict()

	def readValidInCSV(self, fileDir):
		with open(fileDir, 'rb') as CSVFile:
			reader = csv.reader(CSVFile)
			reader.next()

			for row in reader:
				authorId = int(row[0])
				paperIds = row[1]

				paperIds = paperIds.split()

				for i in range(len(paperIds)):
					paperIds[i] = int(paperIds[i])

				self.validInCSV[authorId] = paperIds

	def readValidSolCSV(self, fileDir):
		with open(fileDir, 'rb') as CSVFile:
			reader = csv.reader(CSVFile)
			reader.next()

			for row in reader:
				authorId = int(row[0])
				paperIds = row[1]

				paperIds = paperIds.split()

				for i in range(len(paperIds)):
					paperIds[i] = int(paperIds[i])

				self.validSolCSV[authorId] = paperIds

def main():
	csvData = CSVData()

	curDir = os.getcwd()	

	startT = time.time()

	print 'Reading Valid input CSV'
	csvData.readValidInCSV(os.path.join(curDir, sys.argv[1]))

	tmpT1 = time.time()
	print 'Time elapsed: %f\n' % (tmpT1 - startT)

	print 'Reading Valid Sol CSV'
	csvData.readValidSolCSV(os.path.join(curDir, 'ValidSolution.csv'))

	tmpT2 = time.time()
	print 'Time elapsed: %f\n' % (tmpT2 - tmpT1)

	#print '# authors: %d, # papers: %d\n' % (csvData.authorCnt, csvData.paperCnt)

	print 'Calculate score'

	totAP = 0
	APCnt = 0

	for key, values in csvData.validInCSV.iteritems():
		APCnt += 1

		numer = 0
		denom = len(csvData.validSolCSV[key])
		
		numerNumer = 0
		numerDenom = 0

		for j in range(len(values)):
			numerDenom += 1
			
			if values[j] in csvData.validSolCSV[key]:
				numerNumer += 1
				numer += numerNumer * 1.0 / numerDenom

		totAP += numer / denom

	print 'MAP: %f' % (totAP / APCnt)

if __name__ == '__main__':
	main()
