WITH AuthorJournalCounts AS (
    SELECT AuthorId, JournalId, Count(*) AS Count
    FROM PaperAuthor pa
    LEFT OUTER JOIN Paper p on pa.PaperId=p.Id
    GROUP BY AuthorId, JournalId),
AuthorConferenceCounts AS (
    SELECT AuthorId, ConferenceId, Count(*) AS Count
    FROM PaperAuthor pa
    LEFT OUTER JOIN Paper p on pa.PaperId=p.Id
    GROUP BY AuthorId, ConferenceId),
AuthorPaperCounts AS (
    SELECT AuthorId, Count(*) AS Count
    FROM PaperAuthor
    GROUP BY AuthorId),
PaperAuthorCounts AS (
    SELECT PaperId, Count(*) AS Count
    FROM PaperAuthor
    GROUP BY PaperId),
AuthorPaperYearCounts AS (
    SELECT AuthorId, Year, Count(*) AS Count
    FROM PaperAuthor pa
    LEFT OUTER JOIN Paper p on pa.PaperId=p.Id
    GROUP BY AuthorId, Year),
AuthorPaperDupCounts AS (
    SELECT AuthorId, PaperId, Count(*) AS Count
    FROM PaperAuthor
    GROUP BY AuthorId, PaperId),
--JournalCounts AS (
--    SELECT AuthorId, Count(*) AS Count
--    FROM (
--        SELECT AuthorId, JournalId
--        FROM PaperAuthor pa
--        LEFT OUTER JOIN Paper p on pa.PaperId=p.Id
--        GROUP BY AuthorId, JournalId) AS JournalList
--    GROUP BY AuthorId),
--ConferenceCounts AS (
--    SELECT AuthorId, Count(*) AS Count
--    FROM (
--        SELECT AuthorId, ConferenceId
--        FROM PaperAuthor pa
--        LEFT OUTER JOIN Paper p on pa.PaperId=p.Id
--        GROUP BY AuthorId, ConferenceId) AS ConferenceList
--    GROUP BY AuthorId),
--PaperJournalCounts AS (
--    SELECT PaperId, JournalId, Count(*) AS Count
--    FROM PaperAuthor pa
--    LEFT OUTER JOIN Paper p on pa.PaperId=p.Id
--    GROUP BY PaperId, JournalId),
--PaperConferenceCounts AS (
--    SELECT PaperId, ConferenceId, Count(*) AS Count
--    FROM PaperAuthor pa
--    LEFT OUTER JOIN Paper p on pa.PaperId=p.Id
--    GROUP BY PaperId, ConferenceId),
PaperJournalCounts2 AS (
    SELECT JournalId, Count(*) AS Count
    FROM Paper
    GROUP BY JournalId),
PaperConferenceCounts2 AS (
    SELECT ConferenceId, Count(*) AS Count
    FROM Paper
    GROUP BY ConferenceId),
CoauthorAffiliationCount AS (
    SELECT PaperId, Affiliation, Count(*) AS Count
    FROM PaperAuthor
    GROUP BY PaperId, Affiliation),
SumPapersWithCoAuthors AS (
    WITH CoAuthors AS (
        SELECT pa1.AuthorId Author1, 
               pa2.AuthorId Author2, 
               COUNT(*) AS NumPapersTogether
        FROM PaperAuthor pa1,
             PaperAuthor pa2
        WHERE pa1.PaperId=pa2.PaperId
          AND pa1.AuthorId != pa2.AuthorId
          AND pa1.AuthorId IN (
              SELECT DISTINCT AuthorId
              FROM ##DataTable##)
        GROUP BY pa1.AuthorId, pa2.AuthorId)
    SELECT t.AuthorId,
           t.PaperId, 
           SUM(NumPapersTogether) AS Sum
    FROM ##DataTable## t
    LEFT OUTER JOIN PaperAuthor pa ON t.PaperId=pa.PaperId
    LEFT OUTER JOIN CoAuthors ca ON ca.Author2=pa.AuthorId
    WHERE pa.AuthorId != t.AuthorId
      AND ca.Author1 = t.AuthorId
    GROUP BY t.AuthorId, t.PaperId
),
AggAuthorList AS (
    SELECT PaperId, string_agg(Name, ', ') AS AuthorList
    FROM PaperAuthor
    GROUP By PaperId
)
-- Don't change the order of field of this SELECT query.
-- Features/fields are accessed by index.
SELECT t.AuthorId,
       t.PaperId,
       ajc.Count As NumSameJournal, 
       acc.Count AS NumSameConference,
       apc.Count AS NumPapersWithAuthor,
       pac.Count AS NumAuthorsWithPaper,
       apyc.Count AS NumSameYear,
       CASE WHEN coauth.Sum > 0 THEN coauth.Sum
            ELSE 0 
       END AS SumPapersWithCoAuthors,
       apdc.Count AS NumDupAuthorInPaper,
       --jc.Count AS NumJournal,
       --cc.Count AS NumConference,
       --pjc.Count AS NumAuthorsSameJournal,
       --pcc.Count AS NumAuthorSameConference,
       pjc2.Count AS NumPapersSameJournal,
       pcc2.Count AS NumPapersSameConference,
       cac.Count AS NumCoauthorsSameAffiliation,
       a.name AS TargetAuthorName,
       aal.AuthorList AS AuthorListOfTargetPaper,
       p.title AS TargetPaperTitle,
       p.keyword AS TargetPaperKeyword,
       a.affiliation AS TargetAuthorAffili
FROM ##DataTable## t
LEFT OUTER JOIN Paper p ON t.PaperId=p.Id
LEFT OUTER JOIN Author a ON t.AuthorId=a.Id
LEFT OUTER JOIN AuthorJournalCounts ajc
    ON ajc.AuthorId=t.AuthorId
  AND ajc.JournalId = p.JournalId
LEFT OUTER JOIN AuthorConferenceCounts acc
    ON acc.AuthorId=t.AuthorId
   AND acc.ConferenceId = p.ConferenceId
LEFT OUTER JOIN AuthorPaperCounts apc
    ON apc.AuthorId=t.AuthorId
LEFT OUTER JOIN PaperAuthorCounts pac
    ON pac.PaperId=t.PaperId
LEFT OUTER JOIN AuthorPaperYearCounts apyc
    ON apyc.AuthorId=t.AuthorId
    AND apyc.Year=p.Year
LEFT OUTER JOIN SumPapersWithCoAuthors coauth
    ON coauth.AuthorId=t.AuthorId
   AND coauth.PaperId=t.PaperId
LEFT OUTER JOIN AuthorPaperDupCounts apdc
    ON apdc.AuthorId=t.AuthorId
   AND apdc.PaperId=t.PaperId
LEFT OUTER JOIN AggAuthorList aal
    ON aal.PaperId=t.PaperId
--LEFT OUTER JOIN JournalCounts jc
--    ON jc.AuthorId=t.AuthorId
--LEFT OUTER JOIN ConferenceCounts cc
--    ON cc.AuthorId=t.AuthorId
--LEFT OUTER JOIN PaperJournalCounts pjc
--    ON pjc.JournalId = p.JournalId
--LEFT OUTER JOIN PaperConferenceCounts pcc
--    ON pcc.ConferenceId = p.ConferenceId
LEFT OUTER JOIN PaperJournalCounts2 pjc2
    ON pjc2.JournalId = p.JournalId
LEFT OUTER JOIN PaperConferenceCounts2 pcc2
    ON pcc2.ConferenceId = p.ConferenceId
LEFT OUTER JOIN CoauthorAffiliationCount cac
    ON cac.PaperId = t.PaperId
    AND cac.Affiliation = a.Affiliation
