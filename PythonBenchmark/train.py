import pickle, sys
import data_io
import libter
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import AdaBoostClassifier
from numpy import float32

def calculate_name_feature_TER(target_author_name, target_paper_author_list_str):
    # Feature by Jongmyung
    #print target_paper_author_list_str
    target_paper_author_list = target_paper_author_list_str.split(', ')

    countOfAuthorName = 0.0 
    max_val = 0.0 
    similarity = 0.0;
    #threshold_TER = 0.5
    for an_author_name in target_paper_author_list:
        similarity = libter.ter(target_author_name, an_author_name)
        if (similarity>=max_val):
        #if ((libter.ter(target_author_name, an_author_name) >= threshold_TER)&&(libter.ter(target_author_name, an_author_name)>=max)):
            max_val = similarity
            countOfAuthorName = similarity
            #countOfAuthorName += 1
            #print target_author_name, an_author_name
    
    return countOfAuthorName

    """
    countOfAuthorName = 0
    name_matching_threshold = 0.4
    # counting if the value of measuring matching is over the sepecific threshold.
    for an_author_name in target_paper_author_list:
        if libter.ter(target_author_name, an_author_name) > name_matching_threshold:
            return 1
            #print target_author_name, an_author_name

    # Feature value must be a numerical value.
    return 0
    """

def calculate_name_feature_nameTool(target_author_name, target_paper_author_list_str):
    # Feature by Jinsung
    print target_paper_author_list_str
    target_paper_author_list = target_paper_author_list_str.split(', ')

    countOfAuthorName = 0
    name_matching_threshold = 0.8
    # counting if the value of measuring matching is over the sepecific threshold.
    for an_author_name in target_paper_author_list:
        if name_tools.match(target_author_name, an_author_name) > name_matching_threshold :
            countOfAuthorName += 1
            print target_author_name, an_author_name

    return countOfAuthorName

def get_features():
    print("Getting features for deleted papers from the database")
    features_deleted = data_io.get_features_db("TrainDeleted")

    print features_deleted[0]

    print("Getting features for confirmed papers from the database")
    features_conf = data_io.get_features_db("TrainConfirmed")

    print features_conf[0]

    return [features_deleted, features_conf]

def main():
    print("Given running option: " + str(sys.argv[2:]))
    if len(sys.argv) == 1:
        print("Getting features from DB only.")
        features = get_features()
        features_deleted = features[0]
        features_conf = features[1]
    if sys.argv[1] == "--run-and-dump":
        print("Getting features from DB and saving them to .dump file.")
        features = get_features()
        features_deleted = features[0]
        features_conf = features[1]
        pickle.dump( features_deleted, open("features_deleted.dump", "wb") )
        pickle.dump( features_conf, open("features_conf.dump", "wb") )
    elif sys.argv[1] == "--use-dump":
        print("Loading saved features from .dump file")
        features_deleted = pickle.load( open("features_deleted.dump", "rb") )
        features_conf = pickle.load( open("features_conf.dump", "rb") )
    else:
        print("Invalid running option.")
        sys.exit()

    print("Getting data done.")

    # Calculating feature value by using retrieved fields
    whole_features = features_deleted + features_conf
    for i in xrange(len(whole_features)):
        # Retrieved features are tuples (which are immutable). Convert them to list.
        whole_features[i] = list(whole_features[i])

        # Using target author name & author list of target paper to calculate feature.
        # Calculated feature value is stored to 10th element and used in classification.
        target_author_name = whole_features[i][9+3] # 16th element
        author_list = whole_features[i][10+3] # 17th element
        whole_features[i][9+3] = calculate_name_feature_TER(target_author_name, author_list)

        # Converting fields to float32
        #whole_features[i][9] = whole_features[i][9].astype(float32)
        #whole_features[i][10] = whole_features[i][10].astype(float32)
        #whole_features[i][11] = whole_features[i][11].astype(float32)
        temp = type(whole_features[i][9]).__name__
        if (temp != 'long'):
            whole_features[i][9] = 0
            print "9 " + temp
        temp = type(whole_features[i][10]).__name__
        if (temp != 'long'):
            whole_features[i][10] = 0
            print "10 " + temp
        temp = type(whole_features[i][11]).__name__
        if (temp != 'long'):
            whole_features[i][11] = 0
            print "11 " + temp
        #print temp

        # TODO: impelment other feature-calcuating functions and store them in feature array.
        # For example:
        # feature[10] = calculate_feature(feature[11], feature[12])


    # For now, 3rd ~ 16th elements of 'feature' are used in classification.
    # Reason: Classification only accepts numerial values.
    features = [x[2:13] for x in whole_features]  # use all features
    #features = [(x[2:8] + x[9:13]) for x in whole_features]  # all features except dupcount(9th feature)
    #features = [(x[2:12]) for x in whole_features]  # all features except author name disambiguation
    target = [0 for x in range(len(features_deleted))] + [1 for x in range(len(features_conf))]

    print features[0]
    print("------------------------------------- classification --------------------------------");
    if sys.argv[2] == "-dt" :
        print("decision tree Classifier")
        classifier = DecisionTreeClassifier(max_depth=None, 
                                    min_samples_split=10, 
                                    random_state=1)
    elif sys.argv[2] == "-rf" :
        print("random forest Classifier")
        classifier = RandomForestClassifier(n_estimators=500, 
                                            verbose=2,
                                            n_jobs=-1,
                                            min_samples_split=10,
                                            random_state=1)
    elif sys.argv[2] == "-erf" :
        print("extremely randomized forest Classifier")
        classifier = ExtraTreesClassifier(n_estimators=50, 
                                            verbose=2,
                                            n_jobs=-1,
                                            max_depth=None,
                                            min_samples_split=10, 
                                            random_state=1)
    elif sys.argv[2] == "-gbc" :
        print("gradient boosting Classifier")
        classifier = GradientBoostingClassifier(n_estimators=100,
                                            verbose=2,
                                            learning_rate=1.0,
                                            min_samples_split=10,
                                            max_depth=1, 
                                            random_state=1)
    elif sys.argv[2] == "-ada" :
        print("adaBoosting Classifier")
        classifier = AdaBoostClassifier(n_estimators=100)
    else :
        print("Invalid sys.argv[2].")
        sys.exit()


    classifier.fit(features, target)
    
    print("Saving the classifier")
    data_io.save_model(classifier)
    
if __name__=="__main__":
    main()
