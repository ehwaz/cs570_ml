from collections import defaultdict
import pickle, sys, name_tools
import data_io
import libter

def calculate_name_feature_TER(target_author_name, target_paper_author_list_str):
    # Feature by Jongmyung
    #print target_paper_author_list_str
    target_paper_author_list = target_paper_author_list_str.split(', ')

    countOfAuthorName = 0.0 
    
    max_val = 0.0 
    similarity = 0.0;
    #threshold_TER = 0.5
    for an_author_name in target_paper_author_list:
        similarity = libter.ter(target_author_name, an_author_name)
        if (similarity>=max_val):
        #if ((libter.ter(target_author_name, an_author_name) >= threshold_TER)&&(libter.ter(target_author_name, an_author_name)>=max)):
            max_val = similarity
            countOfAuthorName = similarity
            #countOfAuthorName += 1
            #print target_author_name, an_author_name
    
    return countOfAuthorName

    """
    countOfAuthorName = 0
    name_matching_threshold = 0.4
    # counting if the value of measuring matching is over the sepecific threshold.
    for an_author_name in target_paper_author_list:
        if libter.ter(target_author_name, an_author_name) > name_matching_threshold:
            return 1
            #print target_author_name, an_author_name

    # Feature value must be a numerical value.
    return 0
    """

def calculate_name_feature_nameTool(target_author_name, target_paper_author_list_str):
    # Feature by Jinsung
    target_paper_author_list = target_paper_author_list_str.split(', ')

    countOfAuthorName = 0
    name_matching_threshold = 0.8
    # counting if the value of measuring matching is over the sepecific threshold.
    for an_author_name in target_paper_author_list:
        if name_tools.match(target_author_name, an_author_name) > name_matching_threshold :
            countOfAuthorName += 1
            print target_author_name, an_author_name

    return countOfAuthorName

def main():
    print("Given running option: " + str(sys.argv[1:]))
    if len(sys.argv) == 1:
        print("Getting features for test papers from the database")
        data = data_io.get_features_db("TestPaper")
    if sys.argv[1] == "--run-and-dump":
        print("Getting features for test papers from DB and saving them to .dump file.")
        data = data_io.get_features_db("TestPaper")
        pickle.dump( data, open("features_for_test.dump", "wb") )
    elif sys.argv[1] == "--use-dump":
        print("Loading saved features from .dump file")
        data = pickle.load( open("features_for_test.dump", "rb") )
    else:
        print("Invalid running option.")
        sys.exit()

    print "Before name feature calculation: " + str(data[0])

    for i in xrange(len(data)):
        # Retrieved features are tuples (which are immutable). Convert them to list.
        data[i] = list(data[i])

        # Using target author name & author list of target paper to calculate feature.
        # Calculated feature value is stored to 10th element and used in classification.
        target_author_name = data[i][9+3] # 10th element
        author_list = data[i][10+3] # 11th element
        data[i][9+3] = calculate_name_feature_TER(target_author_name, author_list)

        temp = type(data[i][9]).__name__
        if (temp != 'long'):
            data[i][9] = 0
            print "9 " + temp
        temp = type(data[i][10]).__name__
        if (temp != 'long'):
            data[i][10] = 0
            print "10 " + temp
        temp = type(data[i][11]).__name__
        if (temp != 'long'):
            data[i][11] = 0
            print "11 " + temp

    print "After name feature calculation: " + str(data[0])

    author_paper_ids = [x[:2] for x in data]
    features = [x[2:13] for x in data] # use all features
    #features = [(x[2:8] + x[9:13]) for x in data]  # all features except dupcount(9th feature)
    #features = [(x[2:12]) for x in data]  # all features except author name disambiguation

    print("Loading the classifier")
    classifier = data_io.load_model()

    print("Making predictions")
    predictions = classifier.predict_proba(features)[:,1]
    predictions = list(predictions)

    author_predictions = defaultdict(list)
    paper_predictions = {}

    for (a_id, p_id), pred in zip(author_paper_ids, predictions):
        author_predictions[a_id].append((pred, p_id))

    for author_id in sorted(author_predictions):
        paper_ids_sorted = sorted(author_predictions[author_id], reverse=True)
        paper_predictions[author_id] = [x[1] for x in paper_ids_sorted]

    print("Writing predictions to file")
    data_io.write_submission(paper_predictions)

if __name__=="__main__":
    main()
