#include <cstdlib>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <string>
#include <list>
#include <vector>
#include <set>
#include<map>




//#define MAX_TOK 1000  

extern "C" {
	double ter(char *original, char *paraphrase);
}



double TER(std::string original, const std::string paraphrase); // string 두 개를 입력받아 두 문장간의 TER distance를 출력





int BS(std::string &original, std::string &paraphrase);	// Block shift 구하는 함수	두 스트링을 입력받아 블록시프트 수 리턴
int editdistance(std::string a, std::string b);	// edit distance 구하는 함수 두 스트링을 입력 받아 editdistance를 int 형태로 리턴
int isSame(char a, char b);
int min_T(int a, int b, int c);					//3항 비교연산자
int calculate(std::string * a, std::string *b, int c, int d); 			// edit distance를 구할 때 실제 다이나믹 프로그래밍을 통해 비교하는 과정이 들어있는 함수 형태소 단위로 분리시킨 스트링을 
																		// 이용하여 계산. 
int check(std::string original, std::string paraphrase);				// 두 형태소가 같은지 확인하는 함수 
std::string* StringSplit(std::string strOrigin, std::string strTok);	// 한 문장의 스트링을 



