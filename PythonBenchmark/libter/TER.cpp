#include "TER.h"

#include <algorithm>
#include <stdexcept>
#include <fstream>

#include <cassert>

using namespace std;

// C interface
double ter(char *original, char *paraphrase) {
	return TER(original, paraphrase);
}

double TER(string original, const string paraphrase)
{
	if((original.length() ==0)||(paraphrase.length()==0))
		return 0;
	string buffer_ori = original;						// 원 질문
	string buffer_para = paraphrase;					// 생성 가설

	//cout << buffer_ori << " " << buffer_para << endl;
	int numberofwords = original.length();
	int edit = 0;										// edit 수 
	double value_TER = 0;								
	if(original.length() < paraphrase.length() )
		numberofwords = paraphrase.length();
	
	//cout<<"here\n";
	//std::count(original.begin(), original.end(), ' ') + 1;		// 문장의 형태소 수를 셈
	//cout<<"길이"<<numberofwords<<endl;
	BS(buffer_ori, buffer_para);				// 블록 시프트의 수를 먼저 계산
	//cout<<"here\n";
	//cout << buffer_ori << " " << buffer_para << endl;


	edit += editdistance(buffer_ori, buffer_para);		// BS 계산 후 남은 문장으로 edit distance를 구함 
	//cout<<edit<<endl;
	value_TER = (double)edit / (double)numberofwords;	// 원문장의 형태소 중 얼마나 달라졋는?비율로 출력 
	//cout<<"edits"<<edit<<endl;
	return (1-value_TER);
}

//블록 시프트 함수--------------------------------------------//
int BS(string& original, string& paraphrase)		
{
	
	string *splitOri = StringSplit(original, " ");		//원문장을 어절 단위로 나눔
	string *splitPara = StringSplit(paraphrase, " ");	//패러프레이즈문장을 어절 단위로 나눔

	//cout << "original : " << original << endl;
	//cout << "paraphrase : " << paraphrase << endl;


	int oriSize = std::count(original.begin(), original.end(), ' ') + 1;		//나눠진 어절 덩어리(원문장) 개수 계산
	int paraSize = std::count(paraphrase.begin(), paraphrase.end(), ' ') + 1;	//나눠진 어절 덩어리(패러프레이즈 문장) 개수 계산


	int blockShift = 0;		//블록 시프트 수
	//1. block shift -----------------------------------------------------------------------------------------//
	for (int pointOri = 0; pointOri < oriSize; pointOri++){
		for (int pointPara = 0; pointPara < paraSize; pointPara++){
			if ((splitOri[pointOri].compare(splitPara[pointPara]) == 0) && (pointOri != pointPara)){	//두 문장간 같은 어절이 존재할 경우
				//eliminate the word from sentence
				//original
				for (int i = pointOri; i < oriSize - 1; i++){		//원문장에서 해당 어절 삭제
					splitOri[i] = splitOri[i + 1];
				}
				//paraphrase
				for (int j = pointPara; j < paraSize - 1; j++){		//패러프레이즈문장에서 해당 어절 삭제
					splitPara[j] = splitPara[j + 1];
				}

				//reduce the size of each sentences(original, paraphrase)
				oriSize--;		//원문장 어절수 감소
				paraSize--;		//패러프레이즈문장 어절수 감소


				//restart BS
				blockShift++;	//블록 시프트 실행횟수 증가
				pointOri--;		//다시 앞에서부터 같은 어절 검색하기 위한 포인트 전진
				break;
			}
		}
	}
	//refresh
	//in C, null
	original = "";		//원문장 초기화
	paraphrase = "";	//패러프레이즈문장 초기화

	//cout << "oriSize (after BS): " << oriSize << endl;
	//cout << "paraSize(after BS) : " << paraSize << endl;

	//after block shift, filtered string
	//original
	for (int a = 0; a < oriSize; a++){
		if (a == 0)
			original = splitOri[a];		//블록시프트가 계산된 후 정제된 문장(원문장)의 첫문장을 다시 만듬
		else
			original = original + " " + splitOri[a];	//블록시프트가 계산된 후 정제된 문장(원문장)을 다시 만듬
	}
	//paraphrase
	for (int b = 0; b < paraSize; b++){
		if (b == 0)
			paraphrase = splitPara[b];	//블록시프트가 계산된 후 정제된 문장(패러프레이즈문장)의 첫문장을 다시 만듬
		else
			paraphrase = paraphrase + " " + splitPara[b];	//블록시프트가 계산된 후 정제된 문장(패러프레이즈문장)을 다시 만듬
	}

	//cout << "original(after BS) : " << original << endl;
	//cout << "paraphrase(after BS) : " << paraphrase << endl;


	delete[] splitOri;		//메모리 관리
	delete[] splitPara;
	//cerr<<"BS 수"<<blockShift<<endl;
	return blockShift;
}

//에디트 디스턴스 계산 함수--------------------------------------------//
int editdistance(string original, string paraphrase)
{
	if(original.length() ==0){
		//cout<<paraphrase.length();
		return paraphrase.length();
		
	}
	if(paraphrase.length()==0){
		//cout<<original.length();
		return original.length();
	}
	
	int edit = 0;
	int i, numberofwords_1, numberofwords_2;
	string *words1, *words2;
	stringstream ss, ss2;

	numberofwords_1 = std::count(original.begin(), original.end(), ' ') + 1;		//원문장 길이 계산
	numberofwords_2 = std::count(paraphrase.begin(), paraphrase.end(), ' ') + 1;	//생성문장 길이 계산

	words1 = new string[numberofwords_1];		// 문장을 형태소 단위로 기억하게 될 스트링의 배열 
	words2 = new string[numberofwords_2];

	ss << original;								// 문장을 형태소 단위로 기억하기 위해 스트링 스트림에 일단 입력
	ss2 << paraphrase;

	i = 0;
	while (ss >> words1[i++]);					//문장을 형태소 단위로 저장
	i = 0;
	while (ss2 >> words2[i++]);

	edit = check(original, paraphrase);//calculate(words1, words2, numberofwords_1, numberofwords_2);	//edit distance 계산

	delete[] words1;
	delete[] words2;
	//cerr<<"edit 수"<<edit<<endl;
	return edit;
}
int calculate(string *a, string *b, int c, int d)
{
	int **score;
	int return_value;

	score = new int *[c];
	for (int i = 0; i < c; i++)
		score[i] = new int[d];

	for (int i = 0; i < c; i++)
		score[i][0] = check(a[i], b[0]);	 // 원문장의 첫 형태소와 생성 문장의 모든 형태소 비교
	for (int i = 0; i < d; i++)
		score[0][i] = check(a[0], b[i]);	// 생성 문장의 첫 형태소와 원 문장의 모든 형태소 비교	

	for (int i = 1; i < c; i++)
	{
		for (int j = 1; j < d; j++)
		{
			score[i][j] = min_T(score[i][j - 1] + check(a[i], b[j - 1]), score[i - 1][j] + check(a[i - 1], b[j]), score[i - 1][j - 1] + check(a[i], b[j]));
		}
	}		// delete, insertion, substitution, match 중 어느 것에 해당하는지 고려하여 최소 스코어를 기록

	return_value = score[c - 1][d - 1];

	for (int i = 0; i < c; i++)
		delete score[i];
	delete score;

	return return_value;
}
int check(string a, string b)
{
	int size_a = a.length();
	int size_b = b.length();
	
	int **score;
	int return_value = 0; 

	score = new int *[size_a];
	for (int i = 0; i < size_a; i++)
		score[i] = new int[size_b];

	score[0][0] = isSame(a[0], b[0]);
	for (int i = 1; i < size_a; i++){		
		score[i][0] =score[i-1][0] +isSame(a[i], b[0]);	 // 원문장의 첫 형태소와 생성 문장의 모든 형태소 비교
		if((isSame(a[i],a[i-1]))==0)
			score[i][0] =score[i-1][0];
	}
	for (int i = 1; i < size_b; i++){
		score[0][i] =score[0][i-1]+ isSame(a[0], b[i]);	// 생성 문장의 첫 형태소와 원 문장의 모든 형태소 비교
		if((isSame(b[i],b[i-1]))==0)
			score[0][i] = score[0][i-1];
	}

	for (int i = 1; i < size_a; i++)
	{
		for (int j = 1; j < size_b; j++)
		{
			score[i][j] = min_T(score[i][j - 1] + isSame(a[i], b[j - 1]), score[i - 1][j] + isSame(a[i - 1], b[j]), score[i - 1][j - 1] + isSame(a[i], b[j]));
		}
	}		// delete, insertion, substitution, match 중 어느 것에 해당하는지 고려하여 최소 스코어를 기록
	
		for (int i = 0; i < size_a; i++)
	//{
		//for (int j = 0; j < size_b; j++)
		//{
		//	cout<<score[i][j]<<" ";
		//}cout<<endl;
	//}
	
	
	
	
	
	return_value = score[size_a - 1][size_b - 1];

	for (int i = 0; i < size_a; i++)
		delete score[i];
	delete score;
	
	//cerr<<"캐릭터단위 차이 수"<<return_value<<endl;
	return return_value;
	
	/*
	if (0 == a.compare(b))	//두 스트링이 같은지 확인하는 함수
		return 0;
	else
		return 1;*/
}
int isSame(char a, char b)
{
	if (a==b)
		return 0;
	else 
		return 1;
}

int min_T(int a, int b, int c)
{
	if (a < b)					//3항 비교 연산자 
	{
		if (a < c) return a;
		else return c;
	}
	else
	{
		if (b < c) return b;
		else return c;
	}
}
string* StringSplit(string strOrigin, string strTok)
{
	int     cutAt;
	int     index = 0;
	string* strResult = new string[strOrigin.length()];

	while ((cutAt = strOrigin.find_first_of(strTok)) != strOrigin.npos)
	{
		if (cutAt > 0)
		{
			strResult[index++] = strOrigin.substr(0, cutAt);
		}
		strOrigin = strOrigin.substr(cutAt + 1);
	}

	if (strOrigin.length() > 0)
	{
		strResult[index++] = strOrigin.substr(0, cutAt);
	}


	return strResult;
}


