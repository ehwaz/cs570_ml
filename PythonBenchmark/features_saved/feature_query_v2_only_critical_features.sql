WITH AuthorPaperDupCounts AS (
    SELECT AuthorId, PaperId, Count(*) AS Count
    FROM PaperAuthor
    GROUP BY AuthorId, PaperId)
SELECT t.AuthorId,
       t.PaperId,
       apdc.Count AS NumDupAuthorInPaper
FROM ##DataTable## t
LEFT OUTER JOIN AuthorPaperDupCounts apdc
    ON apdc.AuthorId=t.AuthorId
   AND apdc.PaperId=t.PaperId
