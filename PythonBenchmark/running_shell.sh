#!/bin/bash

echo "$0 $1 $2"

if ["$1" = "-rf"]
then
    echo "random forest... "
elif ["$1" = "-dt"]
then
    echo "decision tree... "
elif ["$1" = "-gbc"]
then
    echo "gradient boosting classifier... "
elif ["$1" = "-erf"]
then
    echo "extremely randomized tree... "
elif ["$1" = "-ada"]
then
    echo "adaBoosting... "
else
    echo "input error..."
    exit
fi

python train.py --use-dump $1
python predict.py --use-dump
